
package keboola.salesforce.extractor.config;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author David Esner <esnerda at gmail.com>
 * @created 2016
 */
public class LastState {

    @JsonProperty("bulkRequests")
    private Map<String, Date> bulkRequests;

    public LastState(Map<String, Date> bulkRequests) {
        this.bulkRequests = bulkRequests;
    }

    public Map<String, Date> getBulkRequests() {
        return bulkRequests;
    }

    public void setBulkRequests(Map<String, Date> bulkRequests) {
        this.bulkRequests = bulkRequests;
    }

    public LastState() {
    }

}